import React, { Component } from "react";
import AddProductModal from "./AddProductModal";

export class Header extends Component {
  constructor() {
    super();  
    this.state = {
      addProductModal: false
    };
  }

  openModal = () => {
    this.setState({ addProductModal: true });
  };

  closeModal = () => {
      this.setState({ addProductModal: false });
  }

  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <button className="btn btn-primary" onClick={this.openModal}>
                Add Product
              </button>
            </li>
            <li className="nav-item active">
              <button className="btn btn-secondary">Add Brand</button>
            </li>
            <li className="nav-item active">
              <button className="btn btn-success">Add Category</button>
            </li>
          </ul>
        </nav>
        {this.state.addProductModal && <AddProductModal closeModal={this.closeModal} />}
      </React.Fragment>
    );
  }
}

export default Header;
