import React, { Component } from "react";
import { connect } from "react-redux";
import { getProducts } from '../actions/products'

export class Product extends Component {

  componentDidMount = () => {
    fetch("http://localhost:5000/products")
      .then(res => res.json())
      .then(res => this.props.getProducts(res));
  };

  render() {
    return <div>
      {
        this.props.products.map(product => (
          <div> { product.key } </div>
        ))
      }
    </div>;
  }
}

const mapStateToProps = (state) => {
  return {
    products: state.products.products
  }
}

const mapActionCreator = {
  getProducts
}

export default  connect(mapStateToProps, mapActionCreator)(Product);
