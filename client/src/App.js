import React from "react";
import { Provider } from "react-redux";
import "./App.css";
import Product from "./components/Product";
import store from "./store";
import Header from "./components/Header";

function App() {
  return (
    <Provider store={store}>
      <div>
        <Header />
        <Product />
      </div>
    </Provider>
  );
}

export default App;
