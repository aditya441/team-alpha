import { FETCH_PRODUCT,NEW_PRODUCT} from './types';

export  const getProducts = (data) => {
    return {
        type: FETCH_PRODUCT,
        products: data
    }
}