const express = require("express");
const cors = require("cors");
const app = express();
const products = require("./server/apipartials/products");
app.use(cors());

app.use('/products', products);
app.use('/categories', require("./server/apipartials/categories"));
app.use('http://localhost:5000/brands', require("./server/apipartials/brand"));


app.listen(process.env.PORT, () => {
  console.log(`Server started at port: ${process.env.PORT}`);
});
