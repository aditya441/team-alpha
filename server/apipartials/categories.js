const express = require('express');

const router = express.Router();

const categoryController = require('../controllers/category.controller.js');

router.route('/')
    // .get(categoryController.search)
    .post(categoryController.create);

// router.route('http://localhost:5000/categories/:id')
//     .get(categoryController.getById)
    

module.exports = router;