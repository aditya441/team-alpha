const express = require('express');

const router = express.Router();

const productController = require('../controllers/product.controller.js');

// router.route('/')
//     .get(productController.search)

//     .post(productController.create);

// router.route('http://localhost:5000/product/:id')
//     .get(productController.getById)

//     .put(productController.updateById)
    
//     .delete(productController.deleteById);    
router.get('/', async (req, res, next) => {
    try {
        const results = await productController.search();
        res.json(results);
      } catch (e) {
        next();
      }
})

module.exports = router;