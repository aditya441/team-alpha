const express = require('express');

const router = express.Router();

const brandController = require('../controllers/brand.controller.js');

router.route('/')
    // .get(categoryController.search)
    .post(brandController.create);

// router.route('http://localhost:5000/categories/:id')
//     .get(categoryController.getById)
    

module.exports = router;