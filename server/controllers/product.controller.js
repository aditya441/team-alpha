const productService = require("../services/product.services");

const search = async request => {
  const product = await productService.getAllProducts();
  return product;
};

const create = async request => {
  const product = await productService.createNewProduct(request.body);
  return product;
};

// const getById = async(request) => {
//     try {
//         const resultProduct = productService.getById(request);
//         res.status(200).send(resultProduct);
//     } catch (error) {
//         res.status(500).send(error);
//     }
// }

// const updateById = async(request) => {
//     try {
//         const resultProduct = productService.updateById(request);
//         res.status(200).send(resultProduct);
//     } catch (error) {
//         res.status(500).send(error);
//     }
// }

// const deleteById = async(request) => {
//     try {
//         const resultProduct = productService.deleteById(request);
//         res.status(200).send(resultProduct);
//     } catch (error) {
//         res.status(500).send(error);
//     }
// }

module.exports = {
  search,
  create
  // getById,
  // updateById,
  // deleteById
};
