const brandService = require('../services/brand.services');

const create = async(request) => {
    try {
        const resultCategory = brandService.create(request);
        res.status(200).send(resultCategory);
    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = {
    create
}