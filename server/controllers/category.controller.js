const categoryService = require('../services/category.services');

const search = async(request) => {
    try {
        const categories = categoryService.search(request);
        res.send(categories);
        } catch (error) {
        res.status(500).send(error);
      }
}

const create = async(request) => {
    try {
        const resultCategory = categoryService.create(request);
        res.status(200).send(resultCategory);
    } catch (error) {
        res.status(500).send(error);
    }
}

const getById = async(request) => {
    try {
        const category = categoryService.getById(request);
        res.status(200).send(category);
    } catch (error) {
        res.status(500).send(error);
    }
}

const updateById = async(request) => {
    try {
        const updatedCategory = categoryService.updateById(request);
        res.status(200).send(updatedCategory);
    } catch (error) {
        res.status(500).send(error);
    }
}

const deleteById = async(request) => {
    try {
        const deletedCategory = categoryService.deleteById(request);
        res.status(200).send(deletedCategory);
    } catch (error) {
        res.status(500).send(error);
    }
}

const getCategoriesAndProductByBrand = async(request) => {
    try {
        const data = categoryService.getCategoriesAndProductByBrand(request);
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send(error);
    }
}

const getProductByCategory = async(request) => {
    try {
        const products = categoryService.getProductByCategory(request);
        res.status(200).send(products);
    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = {
    search,
    create,
    getById,
    updateById,
    deleteById,
    getCategoriesAndProductByBrand,
    getProductByCategory
}

